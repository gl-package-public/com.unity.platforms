using System;
using Unity.Properties.UI;

namespace Unity.Build.Common
{
    public sealed partial class GeneralSettings
    {
        public string ProductName = "Product Name";
        public string CompanyName = "Company Name";
        [SystemVersionUsage(SystemVersionUsage.MajorMinorBuild)]
        public Version Version = new Version(1, 0, 0);
    }
}
